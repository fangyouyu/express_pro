const express = require('express');
const swig = require('swig');
const path = require('path')
const bodyParser = require('body-parser');  //post 请求参数

/*引入express
* */
const app = express();
const port = process.env.PORT || 3600;

/*
* { bodyParser }   res body params
* */
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: false })); // for parsing application/x-www-form-urlencoded

//设置mongoose
require('./config').mongoose

const router = express.Router();
require('./router')(router);

//设置跨域访问
app.use("*", function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header("Access-Control-Allow-Headers", "Content-Type,Content-Length, Authorization, Accept,X-Requested-With");
  res.header("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
  if (req.method === 'OPTIONS') {
    res.send(200)
  } else {
    next()
  }
});


/*
* 设置路由
* */
app.use(router);

//设置swig页面不缓存
swig.setDefaults({
  cache: false
})


/*设置静态目录
* swig
* */
app.set('view cache',false);
app.set('views','./assets/views/pages/');
app.set('view engine','html');
app.engine('html',swig.renderFile);

//静态文件目录，
app.use('/public',express.static('./assets/public'));//将文件设置成静态

/*
*设置404
* */
app.get('*',(req,res) => {
	res.render('404',{
		content:'页面跑丢了'
	})
})


/* start server
* { port }
* */
app.listen(port,err => {
	if(err){
		console.log(err)
		return;
	}
	console.log('server is running at http://127.0.0.1:'+ port);
})