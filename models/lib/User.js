const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const bcrypt = require('bcryptjs');

const UserSchema = new Schema({
    username: { type: String }, // 用户名
    password: { type: String },// 密码
    updateTime: { // 更新时间
        type: String,
        default:Date.parse(new Date)
    },
    img:{
        type: String
    }
});

// 使用pre中间件在用户信息存储前进行密码加密
UserSchema.pre('save', function(next){
    let user = this;

    // 进行加密（加盐）
    bcrypt.genSalt(5, (err, salt) =>{
        if(err){
            return next(err);
        }
        bcrypt.hash(user.password, salt,(err, hash) => {
            if(err){
                return next(err);
            }
            user.password = hash;
            next();
        })
    });
});

/* model 是由schema生成的模型，具有对数据库操作的能力 */

module.exports = mongoose.model('User', UserSchema);