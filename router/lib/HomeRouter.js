var USERCONTROLLER = require('../../controller').USERCONTROLLER;
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();
module.exports = function(router){
	//home
	router.get('/home',USERCONTROLLER.home);

	//zhuce
	router.get('/zhuce',USERCONTROLLER.zhuce);

	//注册
	router.post('/register',USERCONTROLLER.register);

    //登录
    router.post('/login',USERCONTROLLER.login);

    //上传
    router.post('/upload',multipartMiddleware,USERCONTROLLER.upload);

    //验证token
    router.post('/verifyToken',USERCONTROLLER.verifyToken);

    //根据id获取信息
    router.get('/getUserInfo',USERCONTROLLER.getUserInfo);
}