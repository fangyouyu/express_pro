module.exports = {
    send:require('./lib/respons'),
    uid:require('./lib/uid'),
    token:require('./lib/token'),
    key:require('./lib/key'),
    checkToken:require('./lib/checkToken'),
}