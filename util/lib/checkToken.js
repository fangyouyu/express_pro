const jwt = require('jsonwebtoken')
const sendObj = require('./respons');
const key = require('./key');
module.exports = (req,res,callback) =>{
    let token = req.headers.token
    if(token){
        jwt.verify(token, key, (err, decode) => {
            if (err) {  //  时间失效的时候/ 伪造的token
                res.json(sendObj(false, {msg: '登录过期，请重新登录'}));
            } else {
                callback()
            }
        })
    }
}