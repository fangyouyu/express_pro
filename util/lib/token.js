const jwt = require("jsonwebtoken")
const content = {name:"jerry"}
const secretOrPrivateKey = require('./key')
const token = jwt.sign(content, secretOrPrivateKey, {
    expiresIn: 60  // 24小时过期
})

module.exports = token