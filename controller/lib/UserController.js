const util = require('../../util/');
const bcrypt = require('bcryptjs');
const path  = require('path');
const fs  = require('fs');
const USER = require('../../models').USER
const config = require('../../config')

const streamToBuffer = stream => {
    return new Promise((resolve, reject) => {
        let buffers = [];
        stream.on('error', reject);
        stream.on('data', (data) => buffers.push(data))
        stream.on('end', () => resolve(Buffer.concat(buffers)))
    });
}

let UserController = {
    home(req, res) {
        res.render('index');
    },
    zhuce(req, res) {
        res.render('register');
    },
    register(req, res){
        req = req.body || req;
        let params = {
            username: req.username,
            password: req.password
        }
        USER.find({username: req.username}, (err, data) => {
            if (data.length) {
                res.json(util.send(false, {msg: '用户名' + req.username + '已存在'}));
                return;
            }

            let raw = new USER(params);
            raw.save((err, data) => {
                if (err) {
                    res.json(util.send(true, err));
                } else {
                    res.json(util.send(true, {
                        token:util.token,
                        userId:data.id
                    }));
                }
            })
        })
    },
    login(req, res){
        USER.find({username: req.body.username},(err, data) => {
            if(!data.length){
                res.json(util.send(false, {msg: '账户不存在'}));
                return;
            }
            console.log(data[0].password);
            console.log(req.body.password);
            bcrypt.compare(req.body.password, data[0].password).then((f) => {
                if (!f) {
                    res.json(util.send(false, {
                        msg: '密码错误'
                    }));
                } else {
                    res.json(util.send(true, {
                        token:util.token,
                        userId:data[0].id
                    }));
                }
            });
        })

    },
    verifyToken(req, res){
        util.checkToken(req,res,() =>{
            res.json(util.send(true,{}))
        })
    },
    getUserInfo(req, res){
        if(!req.query.userId){
            res.json(util.send(false,{
                msg:'userId不能为空'
            }))
            return;
        }
        USER.findById(req.query.userId,(err, raw) => {
            if (err) console.log(err);
            console.log(raw);
            if(!raw){
                res.json(util.send(false,{
                    msg:'用户信息不存在'
                }))
            }else{
                res.json(util.send(true,raw))
            }
        });
    },
    upload(req, res){
        USER.findById(req.body.userId,(err, raw) => {
            if (err) console.log(err)
            let params = {}
            if(req.body.username){
                params.username = req.body.username
            }
            if(req.files && req.files.imgFile){
                var files  = req.files.imgFile;
                var stream = fs.createReadStream(files.path);
                streamToBuffer(stream).then(data => {
                    let filename = util.uid + '-' + files.name;
                    let imgPath = path.resolve(__dirname, '../../../static-img/img/group1');
                    fs.writeFile(imgPath + '/'+ filename,data,err => {
                        if(err){
                            console.log(err)
                        }else{
                            var url = 'img/group1/' + filename;
                            params.img = url;
                            raw.update(params,(err,newraw) => {
                                if(err){
                                    res.json(util.send(false,{
                                        msg:'图片上传失败'
                                    }))
                                }else{
                                    res.json(util.send(true,newraw))
                                }
                            })
                        }
                    })
                })
                return;
            }
            raw.update(params,(err,newraw) => {
                if(err){
                    res.json(util.send(false,{
                        msg:'修改username失败'
                    }))
                }else{
                    res.json(util.send(true,newraw))
                }
            })

        })
    }
}
module.exports = UserController;