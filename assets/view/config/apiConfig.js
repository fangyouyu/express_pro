
const ENV = require('../env')
const api = {
	DEV:{
		baseUrl:'http://localhost:5000'
	},
	TEST:{
		baseUrl:'http://bbb.com'
	}
}

module.exports = {
	baseUrl:api[ENV.CURRENT].baseUrl
}