import Vue from 'vue'
import Router from 'vue-router'
import INDEX from '@/pages/Index'
import HOME from '@/pages/Home'
import MESSAGE from '@/pages/Message'
import MINE from '@/pages/Mine'
import MESSAGELIST from '@/components/MessageList'

Vue.use(Router)
Router.prototype.goBack = function () { 
　　this.isBack = true
　　window.history.go(-1)
}

export default new Router({
  routes: [
    {
      path: '/',
      redirect:'/home',
      component:INDEX,
      children:[
      	{
	      path: '/home',
	      name: 'HOME',
	      component: HOME,
	      meta:{
	      	index:1
	      }
	    },
	    {
	      path: '/message',
	      name: 'MESSAGE',
	      component: MESSAGE,
	      meta:{
	      	index:2
	      },
	      children:[
	      	{path:'messageList',component:MESSAGELIST,name:MESSAGELIST}
	      ]
	    },
	    {
	      path: '/mine',
	      name: 'MINE',
	      component: MINE,
	      meta:{
	      	index:3
	      }
	    }
      ]
    }
  ]
})
