import Vue from 'vue'
import { 
	XHeader,
	Swiper,
	SwiperItem,
	Tab, 
	TabItem,
	ToastPlugin,
	Popover,
	Group,
	Cell,
	Icon
} from 'vux'

Vue.use(ToastPlugin)






Vue.component('x-header', XHeader)
Vue.component('swiper', Swiper)
Vue.component('swiper-item', SwiperItem)
Vue.component('tab', Tab)
Vue.component('tab-item', TabItem)
Vue.component('popover', Popover)
Vue.component('cell', Cell)
Vue.component('group', Group)
Vue.component('icon', Icon)