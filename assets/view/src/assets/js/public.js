import Vue from 'vue'
import axios from 'axios'
import Api from '../../../config/apiConfig'

Vue.prototype.toast = (msg) => {
	this.$vux.toast.show({
		type: 'text',
	    text: msg,
	    position: 'middle',
	    isShowMask:true
	})
}

Vue.config.productionTip = false

Vue.prototype.$http = axios

Vue.prototype.Api = Api

Vue.prototype.get = (url,params,sucessCallBack,errCallBack)=>{
	return axios.get(url,params)
	.then(function(data){
		sucessCallBack(data.data)
	})
	.catch(function(err){
		errCallBack(err)
	})
}

Vue.prototype.post = (url,params,sucessCallBack,errCallBack)=>{
	return axios.post(url,{
		params:params
	})
	.then(data =>{
		sucessCallBack(data.data)
	})
	.catch(err => {
		errCallBack(err)
	})
}